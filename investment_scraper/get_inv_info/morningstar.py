__author__ = 'olia_boo'

from morning_modules import *
import csv
import os

website = 'http://discuss.morningstar.com'
#os.remove('morningstar_new.csv')

with open('morningstar.csv', 'rb') as csvfile:
    spamreader = csv.reader(csvfile,dialect='excel')
    for row in spamreader:
        row = ', '.join(row)
        term_and_last_message = row.split(',')
        term_and_last_message = ' '.join(term_and_last_message).split()
        if len(term_and_last_message) == 1:
            term_and_last_message.append('')
        print term_and_last_message
        lastPostID = search_term_and_write_to_file(term_and_last_message)
        with open('morningstar_new.csv', 'ab') as csvnewfile:
            spamwriter = csv.writer(csvnewfile,dialect='excel')
            spamwriter.writerow([term_and_last_message[0],lastPostID])
        csvnewfile.close()
csvfile.close()

os.remove('morningstar.csv')
os.rename('morningstar_new.csv', 'morningstar.csv')
