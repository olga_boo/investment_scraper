__author__ = 'olia_boo'

import urllib
from lxml import html
import datetime
from re import sub, compile

website = 'http://discuss.morningstar.com'

def remove_non_ascii(some_str):
    """
    Function which removes all non-ASCII characters from the string
    :param some_str: string non-ASCII characters should be excluded from
    :return: string with ASCII characters only
    """
    some_str = sub(r'[^\x00-\x7F]+',' ', some_str)
    return some_str

def search_term_and_write_to_file(term_and_last_message):
    """
    Function which searches morningstar site for some term and writes the messages containing this term to file.
    If the variable term_and_last_message equals '' it will write the last 10 messages to the file
    :param term_and_last_message: list where first element is the search term and the second element is PostID, where writing to file stops
    :return: lastPostID - id of the last post written to file
    """
    url_ajax = 'http://discuss.morningstar.com/NewSocialize/Utility/AjaxPages/SearchResultsAjax.aspx?q={0}&o=datedescending&pageindex=0&rnd=0.889910986400176'.format(term_and_last_message[0])
    respond = urllib.urlopen(url_ajax).read()

    p = compile('SubjectUrl":"(.*?)"')
    messages_in_file_iterator = 0
    lastPostID = []
    all_links_to_messages = p.findall(respond)
    for link in all_links_to_messages:
        print link
        if term_and_last_message[1] in link and term_and_last_message[1] != '' and term_and_last_message[1] != '0':
            break
        if messages_in_file_iterator >= 10:
            break
        #print link
        title, text = get_text_of_message(link)
        write_message_to_file(website+link, text, title, term_and_last_message[0])
        messages_in_file_iterator += 1
    z = compile('PostID=(.*)')
    if len(all_links_to_messages) > 0:
        lastPostID = z.findall(all_links_to_messages[0])
    #print lastPostID
    if lastPostID == []:
        lastPostID.append(term_and_last_message[1])
    return lastPostID[0]

def get_text_of_message(message_url):
    """
    Function which extracts the title and text of the message
    :param message_url: url of the message in the following format : /smbd.asp?mb=5027&mn=5310&pt=msg&mid=15811903
    :return: list message_title, where message_title[0] is the title
             message_text - list with all the message content
    """
    url = website + message_url
    respond = urllib.urlopen(url).read()
    body = html.fromstring(respond)
    xpath = '//span[@class="textAlt"]/p//text()|//span[@class="textAlt"]/table//td/text()'
    message_title = body.xpath('//span[@class="title16"]/text()')
    message_text = body.xpath(xpath)
    return message_title, message_text

def write_message_to_file(url, message_text, message_title, search_term):
    """
    Function writes url, message title and message content to the file
    stored in test folder with the title morning_<search_term>_<current date - MM.DD.YY>.doc
    :param url: message url
    :param message_text: message content
    :param message_title: message title
    :return:
    """
    print 'writing to file'
    the_date = datetime.date.today().strftime("%B %d, %Y")
    f = open('test/morning_{0}_{1}.doc'.format(search_term, the_date), 'a')
    f.write(url+"\n")
    f.write(remove_non_ascii(message_title[0])+"\n")
    for i in message_text:
        f.write(remove_non_ascii(i)+"\n")
    f.write("*****************************************************************\n")
    f.close()
    return True

