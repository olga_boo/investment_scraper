__author__ = 'olia_boo'

import urllib
from lxml import html
import datetime
from re import sub, compile

website = 'http://www.investorvillage.com'

def remove_non_ascii(some_str):
    """
    Function which removes all non-ASCII characters from the string
    :param some_str: string non-ASCII characters should be excluded from
    :return: string with ASCII characters only
    """
    some_str = sub(r'[^\x00-\x7F]+',' ', some_str)
    return some_str

def search_term_and_write_to_file(forum_id, search_term, lastPostID):
    """
    Function which searches investorvillage site for some term and writes the messages containing this term to file.
    If the variable term_and_last_message[1] equals '' it will write the last 10 messages to the file
    :param term_and_last_message: list where first element is the search term and the second element is PostID, where writing to file stops
    :return: lastPostID - id of the last post written to file
    """
    website = 'http://www.investorvillage.com'
    url = 'http://www.investorvillage.com/smbd.asp?UseArchive=&v=1&category=A&dValue=&rValue=&nmValue=&pmValue=&nhValue=&MLPage=1&PrevNext=0&NewCount=true&pt=m&mb={0}&SearchFor={1}&Subject=&DatePostedMin=&DatePostedMax=&RecommendedBy=&AuthoredBy=&MinRecs=0&FilterType=Search+Results'.format(forum_id, search_term)
    all_links_to_messages = get_links_to_messages(url)
    print all_links_to_messages
    z = compile('mn=(.*?)&pt')
    if all_links_to_messages != []:
        lastPostID_new = z.findall(all_links_to_messages[0])
        for link in all_links_to_messages:
            if lastPostID in link and lastPostID != '0':
                break
            message_title, message_text = get_text_of_message(link)
            write_message_to_file(website + link, message_text, message_title, search_term, forum_id)
        if lastPostID != lastPostID_new[0] and len(lastPostID_new) > 0:
            lastPostID = lastPostID_new[0]
    return lastPostID

def get_links_to_messages(url):
    """
    Function which returns the links to all messages within the web_page
    :param url: url of the webpage to be scraped
    :return: messages_links - list of the links to the messages within the webpage
    """
    messages_links = []
    respond = urllib.urlopen(url).read()
    body = html.fromstring(respond)
    more_messages = body.xpath('//span[@class="reset fwb"]/text()')
    #print more_messages
    if more_messages[0] == '0':
        return []
    all_hrefs = body.xpath('//a/@href')
    for i in all_hrefs:
        if "msg&mid" in i:
            messages_links.append(i)
    return messages_links

def get_text_of_message(message_url):
    """
    Function which extracts the title and text of the message
    :param message_url: url of the message in the following format : /smbd.asp?mb=5027&mn=5310&pt=msg&mid=15811903
    :return: list message_title, where message_title[0] is the title
             message_text - list with all the message content
    """
    url = website + message_url
    respond = urllib.urlopen(url).read()
    body = html.fromstring(respond)
    #xpath = '//div[@id="divMessageText"]/div/text()|//div[@id="divMessageText"]/div/span/text()|//div[@id="divMessageText"]/span/text()|//div[@id="divMessageText"]/text()|//div[@id="divMessageText"]/p/text()'
    xpath = '//div[@id="divMessageText"]//text()'
    message_title = body.xpath('//h1[@class="fs14 pB10"]/text()')
    message_text = body.xpath(xpath)
    return message_title, message_text

def write_message_to_file(url, message_text, message_title, search_term, forum_id):
    """
    Function writes url, message title and message content to the file
    :param url: message url
    :param message_text: message content
    :param message_title: message title
    :param forum_id: forum id
    :return:
    """
    the_date = datetime.date.today().strftime("%B %d, %Y")
    f = open('test/investor_{0}_{1}_{2}.doc'.format(forum_id, search_term, the_date), 'a')
    f.write(url+"\n")
    f.write(remove_non_ascii(message_title[0])+"\n")
    for i in message_text:
        f.write(remove_non_ascii(i)+"\n")
    f.write("*****************************************************************\n")
    f.close()
    return True

#TODO
#add exception handling when writing to file

#The end of search for the first time - look for:
#<span>There were no messages found. Refine your search and try again.</span>
#find out the way to remember the lase message parsed before.
