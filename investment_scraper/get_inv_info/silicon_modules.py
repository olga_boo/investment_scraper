__author__ = 'olia_boo'

import urllib
from lxml import html
import datetime
from re import sub, compile

def text(elt):
    return elt.text_content().replace(u'\xa0', u' ')

def remove_non_ascii(some_str):
    """
    Function which removes all non-ASCII characters from the string
    :param some_str: string non-ASCII characters should be excluded from
    :return: string with ASCII characters only
    """
    some_str = sub(r'[^\x00-\x7F]+',' ', some_str)
    return some_str

def search_term_and_write_to_file(forum_id, search_term, lastPostID):
    """
    Function which executes forum-specific search siliconinvestor site for some term and writes the messages containing this term to file.
    :param search_term:
           forum_id:
           lastPostID:
    :return: lastPostID - id of the last post written to file
    """
    url = 'http://www.siliconinvestor.com/subject.aspx?subjectid={0}&NumMsgs=100'.format(forum_id)
    message_links = []
    respond = urllib.urlopen(url).read()
    body = html.fromstring(respond)
    all_links = body.xpath('//table[@id="grdMsgList"]//a/@href')
    for i in all_links:
        if 'msgid' in i:
            message_links.append(i)
    #print len(message_links)
    message_links, messages_text, messages_titles = check_term_in_message(message_links, search_term)
    print message_links
    if len(message_links) > 0:
        z = compile('msgid=(.*)')
        lastPostID_new = z.findall(message_links[0])
        for i in range(len(message_links)):
            print message_links[i]
            if lastPostID in message_links[i] and lastPostID != '0':
                break
            if len(messages_titles[i]) == 0:
                messages_titles[i].append("")
                print messages_titles[i]
            write_message_to_file(message_links[i], messages_titles[i][0],messages_text[i],forum_id, search_term)
        if lastPostID != lastPostID_new[0] and len(lastPostID_new) > 0:
            lastPostID = lastPostID_new[0]
    print lastPostID
    return lastPostID

def get_last_reply_num(url):
    """
    Fuction which finds the last reply id
    needed for the result messages paginating
    return: last number for the next searched page:
    http://www.siliconinvestor.com/subject.aspx?subjectid=58607&LastNum=<last_number>&NumMsgs=25
    """
    msg_replies = []
    respond = urllib.urlopen(url).read()
    body = html.fromstring(respond)
    for table in body.xpath('//table[@id="grdMsgList"]/tr'):
        data = [td for td in table.xpath('td')]
        if len(data) > 0:
            msg_replies.append(data[0])
    return int(msg_replies[len(msg_replies)-1]) - 1

def check_term_in_message(message_links, searched_term):
    """
    Function which returns the links to the messages containing particular search term
    :param message_links: the links to the messages within the web-page
           searched_term: the term which message content is looked for
    :return: messages_with_term - list of the links to the messages containing the specified term
    """
    website = 'http://www.siliconinvestor.com/'
    messages_with_term = []
    messages_text = []
    messages_titles = []
    for i in message_links:
        respond = urllib.urlopen(website + i).read()
        if searched_term in respond:
            messages_with_term.append(website + i)
            body = html.fromstring(respond)
            messages_text.append(body.xpath('//span[@id="intelliTXT"]/text()'))
            messages_titles.append(body.xpath('//span[@id="intelliTXT"]/b/text()'))
    return messages_with_term, messages_text, messages_titles


def write_message_to_file(message_link, message_title, message_text, forum_id, search_term):
    """
    Function writes url, message title and message content to the file
    :param message_text: message content
    :param message_title: message title
    :return:
    """
    the_date = datetime.date.today().strftime("%B %d, %Y")
    f = open('test/silicon_{0}_{1}_{2}.doc'.format(forum_id, search_term, the_date), 'a')
    f.write(message_link + "\n")
    #print message_title
    f.write(remove_non_ascii(message_title) + "\n")
    for i in message_text:
        f.write(remove_non_ascii(i))
    f.write("\n")
    f.write("*****************************************************************\n")
    f.close()
    return True
