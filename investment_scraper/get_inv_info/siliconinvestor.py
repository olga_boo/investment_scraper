__author__ = 'olia_boo'

from silicon_modules import *
import csv
import os

def process_data(excel_data):
    """
    Function which processes search_forums and search_terms and transfer this data to search_term_and_write_to_file function
    :param excel_data: list of lists; excel_data[0] - search forums ids.
    :return: excel_data with updated lastPostID
    """
    num_of_forums = len(excel_data[0])
    num_of_terms = len(excel_data) - 1
    for forum in range(num_of_forums):
        for term in range(num_of_terms):
            print excel_data[0][forum], excel_data[term+1][0],excel_data[term+1][forum+1]
            lastPostID = search_term_and_write_to_file(excel_data[0][forum], excel_data[term+1][0],excel_data[term+1][forum+1])
            excel_data[term+1][forum+1] = lastPostID
            print lastPostID
            print type(lastPostID)
    excel_data[0].insert(0,'')
    return excel_data

with open('siliconinvestor.csv', 'rb') as csvfile:
    spamreader = csv.reader(csvfile,dialect='excel')
    excel_data = []
    for row in spamreader:
        row = ', '.join(row)
        term_and_last_message = row.split(',')
        term_and_last_message = ' '.join(term_and_last_message).split()
        excel_data.append(term_and_last_message)
csvfile.close()

excel_data_new = process_data(excel_data)

with open('siliconinvestor_new.csv', 'ab') as csvnewfile:
    spamwriter = csv.writer(csvnewfile,dialect='excel')
    for i in excel_data:
        spamwriter.writerow(i)
csvnewfile.close()

os.remove('siliconinvestor.csv')
os.rename('siliconinvestor_new.csv', 'siliconinvestor.csv')
